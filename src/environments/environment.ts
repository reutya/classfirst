// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB6GOKFFGyMqf6V1GMPhFnOuG5WECIw1Gw",
    authDomain: "books-c6a09.firebaseapp.com",
    databaseURL: "https://books-c6a09.firebaseio.com",
    projectId: "books-c6a09",
    storageBucket: "books-c6a09.appspot.com",
    messagingSenderId: "368858818862",
    appId: "1:368858818862:web:a90a61f1afd5f708a6f2bf",
    measurementId: "G-Q1ZMLC38Q8"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
