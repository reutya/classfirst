import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from './../books.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  constructor(private booksservice:BooksService, private router:Router ,private route:ActivatedRoute,public authService :AuthService) { }
 
  title:string;
  author:string; 
  id:string;
  isEdit:boolean=false;
  buttonText:string="Add Book";
  userId:string;


  onSubmit(){ 
    if(this.isEdit){
      this.booksservice.updateBook(this.userId,this.id,this.title,this.author);
    }else{
      this.booksservice.addBook(this.userId,this.title,this.author);
    }
    
    this.router.navigate(['/books']);
  }
  
  
  ngOnInit() {
    this.id=this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user=>{
        this.userId = user.uid;
        if(this.id){
          this.isEdit=true;
          this.buttonText="Update book";
          this.booksservice.getBook(this.id,this.userId).subscribe(
            book=>{
              this.author=book.data().author;
              this.title=book.data().title;
            }
          )
        }
    
      }
    )
    console.log(this.id);
    // if(this.id){
    //   this.isEdit=true;
    //   this.buttonText="Update book";
    //   this.booksservice.getBook(this.id).subscribe(
    //     book=>{
    //       this.author=book.data().author;
    //       this.title=book.data().title;
    //     }
    //   )
    // }




  }

}
