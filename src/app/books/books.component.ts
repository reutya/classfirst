import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;

  books:any;
  userId:string;
  books$:Observable<any>;


  deleteBook(id:string){
    this.booksservice.deleteBook(id,this.userId);
  }
  constructor(private booksservice:BooksService,
    public authService:AuthService) { }

  ngOnInit() {

this.authService.user.subscribe(
  user=>{
    this.userId=user.uid;
    this.books$ = this.booksservice.getBooks(this.userId);
  }
)


    /*this.books= this.booksservice.getBooks().subscribe(
      (books)=>this.books=books
    );*/

    //שיעור 10this.books$=this.booksservice.getBooks();
   // this.booksservice.addBooks();
  }


}
