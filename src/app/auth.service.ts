import { User } from './interfaces/user';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth,private router:Router) {
    this.user = this.afAuth.authState;
  }

  Signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(() =>
          //console.log('Succesful sign up',res)
         {this.router.navigate(['/books'])}
        );

  }

  Logout(){
    this.afAuth.auth.signOut().then(res=>console.log('succses logout'))

  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          // res => console.log('Succesful Login',res)
         // res => console.log(this.user.subscribe(user=>console.log(user.uid)))
         user=>{this.router.navigate(['/books'])}
        )
  }



}
