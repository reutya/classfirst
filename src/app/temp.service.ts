// import { WeatherRaw } from './interfaces/weather-raw';
// import { Weather } from './interfaces/weather';
// import { Injectable } from '@angular/core';
// import{HttpClient} from '@angular/common/http';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';


// @Injectable({
//   providedIn: 'root'
// })
// export class TempService {
//   private URL = "http://api.openweathermap.org/data/2.5/weather?q="
//   private KEY="f2b36a5f52f5d27dab72f47e81ac6846‏";
//   private IMP= "&units=metric";

// searchWeatherData(cityName:String):Observable<Weather>{
//   return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
//   .pipe(map(data=>this.transformWeatherData(data)))
// }

// private transformWeatherData(data:WeatherRaw):Weather{
//   return {
//     name:data.name,
//     country:data.sys.country,
//     image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
//     description:data.weather[0].description,
//     temperature:data.main.temp,
//     lat:data.coord.lat,
//     lon:data.coord.lon
//   }
// }
  

//   constructor(private http:HttpClient) { }
// }

import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';
//import { WeatherData } from './../shared/interfaces/weather-data';
//import { Weather } from './../shared/interfaces/weather';

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "f2b36a5f52f5d27dab72f47e81ac6846";
  private IMP = "&units=metric";

//  searchWeatherData(cityName:string): Observable<Weather> {
//   console.log(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`);
//   return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
//       .pipe(
//         map(data => this.transformWeatherData(data))
//       )
//  }
  
private handleError(res:HttpErrorResponse){
  //console.log("in the service" + res.error)
  console.log(res.error);
  return throwError(res.error);

}

searchWeatherData(cityName:string): Observable<Weather> {
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
                    .pipe(
                      map(data => this.transformWeatherData(data)),
                      catchError(this.handleError)

                    )  
 }


 constructor(private http: HttpClient) { }
  
/*
 private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return throwError(res.error || 'Server error');
  }
*/
  private transformWeatherData(data:WeatherRaw):Weather {
    return {
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature: data.main.temp,
      lat: data.coord.lat,
      lon: data.coord.lon
    }
  }

}