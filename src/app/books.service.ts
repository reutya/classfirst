import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatGridTileHeaderCssMatStyler } from '@angular/material';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
  
})

export class BooksService {
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection ;
//books:any =[{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];

// getBooks(){
//   const bookObservable = new Observable(
//     observer=>{
//       setInterval(
//        ()=> observer.next(this.books),5000
//       )
//     }
//   )
//   return bookObservable
// }

getBooks(userId:string):Observable<any[]>{
 //return this.db.collection('books').valueChanges({idField:'id'});
  this.bookCollection=this.db.collection(`users/${userId}/books`);
  return this.bookCollection.snapshotChanges().pipe(
    map(
      collection=>collection.map(
        document=>{
          const data= document.payload.doc.data();
          data.id= document.payload.doc.id;
          console.log(data);
          return data;
        }
      )
    )
  )
}
getBook(id:string,userId:string):Observable<any>{
  //return this.db.doc(`books/${id}`).get();
  return this.db.doc(`users/${userId}/books/${id}`).get();
 }
 
 addBook(userId:string,title_input:string,author:string){
  const book= {title:title_input,author:author};
  //this.db.collection('books').add(book);
  this.userCollection.doc(userId).collection('books').add(book);
 }

// addBook(title_input:string,author:string){
//   const book= {title:title_input,author:author};
//   this.db.collection('books').add(book);
// }





// addBooks(){
//   setInterval(
//     ()=>this.books.push({title:"harry",author:"jkr"})
//   ,5000)
// }

deleteBook(id:string,userId:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}

updateBook(userId:string,id:string,title:string,author:string){
  this.db.doc(`users/${userId}/books/${id}`).update({title:title,author:author})

}
/*
getBooks(){
  setInterval(()=> this.books,1000)
}  
*/

constructor(private db:AngularFirestore) { }
}
